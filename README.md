# Vodyatel API

## Requirements
install docker:  
https://docs.docker.com/engine/install/  
  
install docker compose:  
https://docs.docker.com/compose/install/  

install node js:  
https://nodejs.org/en/download/  
## Install
run npm install
____________________________________

## First run and when DB dump updates (migrate DB)
npm run migrate
____________________________________

## Start
npm start  
____________________________________

## Create dump (DBeaver)
Export configuration -> Extra command args (input) "--column-statistics=0"  
  

If you need to recreate db:
docker rm $(docker ps -a -q) -f  
docker volume prune
