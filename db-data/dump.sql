-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: 0.0.0.0    Database: vodyatel
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `car_numbers`
--

DROP TABLE IF EXISTS `car_numbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `car_numbers` (
  `id` varchar(100) NOT NULL,
  `car_number` varchar(100) NOT NULL,
  `is_removed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `car_numbers_UN` (`car_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_numbers`
--

LOCK TABLES `car_numbers` WRITE;
/*!40000 ALTER TABLE `car_numbers` DISABLE KEYS */;
INSERT INTO `car_numbers` VALUES ('cs-5a7c4ac7-0163-4e7f-bc9b-b8694ef97bd9','е023ор154',0),('cs-7981c081-cd7f-42e1-ad18-ee87cede4165','е787ес14',0),('cs-8ddb0cff-d668-4f66-8955-f568cc620ade','р235но154',0),('cs-99edc80b-7d34-4952-95d1-0b62f4460a38','в354ак154',0),('cs-cc92c6b6-75bc-4704-987b-9e8144de550a','е023ор54',0);
/*!40000 ALTER TABLE `car_numbers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `id` varchar(100) NOT NULL,
  `car_number_id` varchar(100) NOT NULL,
  `text` varchar(350) CHARACTER SET utf8mb4 NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `user_name` varchar(100) DEFAULT '' COMMENT 'If is not empty - review was send by anonymous',
  `is_removed` tinyint(1) DEFAULT '0',
  `location_latitude` float DEFAULT NULL,
  `location_longitude` float DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `reviews_FK` (`car_number_id`),
  CONSTRAINT `reviews_FK` FOREIGN KEY (`car_number_id`) REFERENCES `car_numbers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES ('ddrs-cc841fd8-3b99-4722-b765-0a8993eb7b0b','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','ооооооо Тестовый текст отзыва',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-12 12:52:48'),('rs-2ff57ce0-3d44-4826-98b2-14d953095623','cs-5a7c4ac7-0163-4e7f-bc9b-b8694ef97bd9','вцйвцйвцвцйвцвццвйвцйцвй123231',3,'Alex_Farshatov',0,NULL,NULL,'2021-04-11 15:18:26'),('rs-5a1dd73a-ca48-4e29-ad77-d553b0042a4a','cs-8ddb0cff-d668-4f66-8955-f568cc620ade','Насрал на дороге',0,'Polina_Korotenkova',0,55.0809,82.931,'2021-04-11 14:29:49'),('rs-672dc69f-bcc1-4ea8-9c42-f63cac47429d','cs-5a7c4ac7-0163-4e7f-bc9b-b8694ef97bd9','Ппррол',0,'Alex_Farshatov',0,55.0436,82.9299,'2021-04-12 12:11:07'),('rs-b29a85f0-74e1-49d7-b851-4644009d6f82','cs-7981c081-cd7f-42e1-ad18-ee87cede4165','НСК, обеденное время. Поставила машину таким образом, что заблокировала нескольким машинам выезд с парковки.',0,'m1ronychev',0,54.9321,82.9237,'2021-04-11 15:19:16'),('rs-c569c9e3-7387-4ddf7-8825-73274b1686c1','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','Тестовый текст отзыва',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-12 12:52:48'),('rs-c569c9e3-7387-4df7-8825-73274b1686c1','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','Тестовый текст отзыва',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-11 14:10:15'),('rs-c569c9e3-87-4df27-8825-73274b1686c1','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','ррвцйрйцв Тестовый текст отзыва вццйвр',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-12 12:52:48'),('rs-c569c9e3-87-4df7-8825-73274b1686c1','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','ррвцйрйцв Тестовый текст отзыва вццйвр',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-11 19:10:15'),('rs-cc841f-3b99-4722-b765-0a8993eb7b0b','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','вцйвцйцвйвйцйцв Тестовый текст отзыва',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-11 20:10:15'),('rs-cc841fd8-3b99-4722-b765-0a8993eb7b0','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','123321321231 Тестовый текст отзыва',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-11 17:10:15'),('rs-cc841fd8-3b99-4722-b765-0a8993eb7b0b','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','ооооооо Тестовый текст отзыва',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-11 16:10:15'),('rs-cc841fd8-3b99-b765-0a8993eb7b0b','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','аааааааааа Тестовый текст отзыва',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-11 18:10:15'),('rs-cc841fd8-3b994722-b765-0a8993eb7b0b','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','Тестовый текст отзыва у21у21у21у21',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-11 15:10:15'),('rs-cc841fd8-3b99dddd4722-b765-0a8993eb7b0b','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','Тестовый текст отзыва у21у21у21у21',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-12 12:52:48'),('rs-cc841fdddd8-3b99-b765-0a8993eb7b0b','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','аааааааааа Тестовый текст отзыва',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-12 12:52:48'),('rs-cc8ddd41f-3b99-4722-b765-0a8993eb7b0b','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','вцйвцйцвйвйцйцв Тестовый текст отзыва',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-12 12:52:48'),('rs-cddc841fd8-3b99-4r722-b765-0a8993eb7b0','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','123321321231 Тестовый текст отзыва',5,'Alex_Farshatov',0,NULL,NULL,'2021-04-12 12:52:48'),('rs-e015f7ba-afeb-470e-99de-6d0d0f6a2f46','cs-99edc80b-7d34-4952-95d1-0b62f4460a38','Тестовый отщыв)))😂',5,'Alex_Farshatov',0,55.0432,82.929,'2021-04-12 12:57:06');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_removed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_unique_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_car_numbers`
--

DROP TABLE IF EXISTS `users_car_numbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_car_numbers` (
  `id` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `car_number_id` varchar(100) NOT NULL,
  KEY `users_car_numbers_FK` (`car_number_id`),
  KEY `users_car_numbers_FK_1` (`user_id`),
  CONSTRAINT `users_car_numbers_FK` FOREIGN KEY (`car_number_id`) REFERENCES `car_numbers` (`id`),
  CONSTRAINT `users_car_numbers_FK_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_car_numbers`
--

LOCK TABLES `users_car_numbers` WRITE;
/*!40000 ALTER TABLE `users_car_numbers` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_car_numbers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_info`
--

DROP TABLE IF EXISTS `users_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_info_FK` (`user_id`),
  CONSTRAINT `users_info_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_info`
--

LOCK TABLES `users_info` WRITE;
/*!40000 ALTER TABLE `users_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'vodyatel'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-12 19:58:09
