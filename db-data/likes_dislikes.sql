CREATE TABLE vodyatel.reviews_users_likes (
	id varchar(100) NOT NULL,
	user_id varchar(100) NOT NULL,
	review_id varchar(100) NOT NULL,
	is_liked tinyint(1) DEFAULT '0' NULL,
	is_disliked tinyint(1) DEFAULT '0' NULL,
	is_removed tinyint(1) DEFAULT '0',
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;



ALTER TABLE vodyatel.reviews_users_likes ADD CONSTRAINT reviews_users_likes_FK FOREIGN KEY (review_id) REFERENCES vodyatel.reviews(id);
