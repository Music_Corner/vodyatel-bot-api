import fs from 'fs';
import path from 'path';
import { configureEnv } from '../configureEnv';
import initMysql from '../src/services/mysqlConnection';

const DUMP_PATH = path.resolve(__dirname, 'dump.sql');
configureEnv();

const migrate = async () => {
	console.log('Started migrating DB from', DUMP_PATH);

	fs.readFile(DUMP_PATH, 'utf8', async (err, data) => {
		try {
			if (err) throw err;

			const queryString = data.toString('ascii');

			await initMysql().query(queryString);
			console.log('DB migrated successfully!');
			initMysql().end();

			return { data: 'success' };
		} catch (error) {
			console.error('DB Migration error', error);
		}
	});
};

migrate();