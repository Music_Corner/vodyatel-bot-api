import path from 'path';
import dotenv from 'dotenv';

import { ENV } from './src/common/constants/env';

export const configureEnv = () => {
	const options = !ENV.IS_DEV() && {
		path: path.resolve(process.cwd(), '.production.env'),
	};
	
	dotenv.config(options);
};
