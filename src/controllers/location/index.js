import { Controller } from '..';
import LocationService from '../../services/location';

export default class LocationController extends Controller {
	locationService = new LocationService();

	async getLocationString(req, res) {
		const location = this.getParsedQueryParams(req, res);
		const locationString = await this.locationService.getLocationString(location);

		return locationString;
	}

	getLocationLink(req, res) {
		const location = this.getParsedQueryParams(req, res);
		const locationString = this.locationService.getLocationLink(location);

		return locationString;
	}
}
