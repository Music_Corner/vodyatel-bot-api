import { compare } from 'bcrypt';
import { Controller } from '..';
import { STATUSES } from '../../common/constants/api';
import { API_ERROR_CODES, ERROR_MESSAGES } from '../../common/constants/errors';
import { DB_NAMES } from '../../common/constants/mysql';
import PrivateUserModel from '../../models/users/PrivateUserModel';
import MailSerivce from '../../services/mail';
import UsersService from '../../services/users';

const usersService = new UsersService();

export default class UsersController extends Controller {
	async registerUser(req, res) {
		const form = this.getParsedBody(req);
		const { password, confirmation, login } = form;

		if (password && password !== confirmation) throw ERROR_MESSAGES.PASSWORDS_ARE_NOT_SAME;

		await usersService.createUser(form);

		try {
			await new MailSerivce().sendMail(login, 'verification code is: 1325', 'Eamil Verification');
		} catch (err) {
			return { message: 'Email senting failed..', err };
		}
	}

	async loginUser(req, res) {
		const form = this.getParsedBody(req);
		const { login, password } = PrivateUserModel.validate(form);

		const authError = {
			status: STATUSES.AUTHORIZATION_ERROR,
			message: 'Credentials are incorrect',
			errors: [{ code: API_ERROR_CODES.SECURITY.INVALID_AUTH_DATA }]
		};

		const users = await usersService.getUsers(DB_NAMES.COLUMNS.EMAIL, login);

		if (!users.length) throw authError;

		const [user = {}] = users;
		const { password: _password = '', id = '' } = user;

		const doPasswordsMatch = await compare(password, (_password));

		if (doPasswordsMatch) {
			const userInfo = await usersService.getUserInfo(id);
			const accessToken = await usersService.generateToken(user);

			return { ...userInfo, accessToken };
		}

		throw authError;
	}
}
