import { Controller } from '..';
import ReviewModel from '../../models/reviews/ReviewModel';
import ReviewsService from '../../services/reviews';

export default class ReviewsController extends Controller {
	reviewsService = new ReviewsService();

	async getReviewById(req, res) {
		const { id } = this.getParsedRouteParams(req, res);

		const { items: reviewsItems, count } = await this.reviewsService
			.getReviews({ id });

		const items = reviewsItems.map(review => new ReviewModel(review));

		return items && items[0];
	}

	async getReviews(req, res) {
		const { items: reviewsItems, count } = await this.reviewsService
			.getReviews(this.getParsedQueryParams(req));

		const items = reviewsItems.map(review => new ReviewModel(review));

		return { items, count };
	}

	async addReview(req, res) {
		const { review } = this.getParsedBody(req);
		const result = await this.reviewsService.addReview(review);

		return result;
	}

	async likeDislikeReview(req, res, like) {
		const { id } = this.getParsedRouteParams(req);
		const { userId } = this.getParsedBody(req);

		await this.reviewsService.likeDislikeReview(id, userId, like);
	}
}
