import mysql from 'mysql2';

let connetcion;

const initMysql = () => {
	if (!connetcion) {
		const MYSQL_CONFIG = {
			host: '0.0.0.0',
			port: 3307,
			user: process.env.DB_USERNAME,
			password: process.env.DB_PASSWORD,
			database: 'vodyatel',
			connectionLimit: 10,
			multipleStatements: true,
		};

		connetcion = mysql.createPool(MYSQL_CONFIG).promise();
	}

	return connetcion;
};

export default initMysql;
