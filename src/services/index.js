import { v4 as uuidv4 } from 'uuid';

import { GENERATED_ERRORS } from '../common/constants/errors';
import { DB_NAMES } from '../common/constants/mysql';
import { getLimitOperator } from '../common/utils/helpers';
import initMysql from './mysqlConnection';

export default class Service {
	async removeInstanceSoftly(tableName, id) {
		await initMysql().query(`
		UPDATE ${tableName}
		SET ${DB_NAMES.COLUMNS.IS_REMOVED} = 1
		WHERE ${DB_NAMES.COLUMNS.ID} = ${id}
	`)
	}

	async getSingleInstanceByColumn(tableName, columnName, value) {
		const _value = typeof value === 'number' ? _value : `"${value}"`;

		const [[result]] = await initMysql().query(`
			SELECT * FROM ${tableName}
			WHERE ${columnName} = ${_value}
			AND ${DB_NAMES.COLUMNS.IS_REMOVED} = 0
		`);

		if (!result) {
			throw GENERATED_ERRORS.UNPROCESSABLE_ENTITY
		}

		return result;
	}

	async getInstances(tableName, page = 1, limit = 0, additionalWhereClause = '') {
		const limitOperator = getLimitOperator(page, limit);

		const [items] = await initMysql().query(`
			SELECT * FROM ${tableName}
			WHERE ${DB_NAMES.COLUMNS.IS_REMOVED} = 0
			${additionalWhereClause ? `AND ${additionalWhereClause}` : ''}

			${limitOperator}
		`);

		return items;
	}

	async insertInstance(tableName, columnsToInsert = [], valuesToInsert = []) {
		const restInsertColumnsClause = columnsToInsert.length
			? columnsToInsert.reduce((accum, value) => `${accum}, ${value}`, '')
			: '';

		const restInsertValuesClause = valuesToInsert
			? valuesToInsert
				.reduce((accum, value) => (
					`${accum}, ${typeof value === 'number' ? value : `"${value}"`}`
				), '')
			: '';

		const insertId = `${tableName[0]}${tableName[tableName.length - 1]}-${uuidv4()}`;

		const query = `
			INSERT INTO ${tableName}
			(${DB_NAMES.COLUMNS.ID} ${restInsertColumnsClause})
			VALUES
			("${insertId}" ${restInsertValuesClause});
		`;

		const [res] = await initMysql().query(query);

		return { ...(res || {}), insertId };
	}

	async updateInstance(
		tableName = '',
		columns = [],
		values = [],
		whereClause = ''
	) {
		const valuesClause = values
			.reduce((accum, value, index) => (
				`${accum + (index ? ',' : '')} ${columns[index]} = ${value}`
			), '');

		await initMysql().query(`
			UPDATE ${tableName}
			SET ${valuesClause}
			${whereClause}
		`);
	}
}
