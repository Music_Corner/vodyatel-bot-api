import { DB_NAMES } from '../../common/constants/mysql';
import { getLimitOperator } from '../../common/utils/helpers'
import initMysql from '../mysqlConnection';

export const getReviews = async ({ page, limit, carNumber, id }) => {
	const limitOperator = getLimitOperator(page, limit);
	const whereReviewId = id
		? `${carNumber ? 'AND ' : 'WHERE '}reviews_t.${DB_NAMES.COLUMNS.ID} = "${id}"`
		: '';

	const whereOperator = carNumber
		? `
			WHERE numbers_t.${DB_NAMES.COLUMNS.CAR_NUMBER} = "${carNumber}"
			${whereReviewId}
		`
		: whereReviewId;

	const mainSqlQuery = `
		FROM ${DB_NAMES.TABLES.REVIEWS} as reviews_t
		JOIN ${DB_NAMES.TABLES.CAR_NUMBERS} as numbers_t
		ON reviews_t.${DB_NAMES.COLUMNS.CAR_NUMBER_ID} = numbers_t.${DB_NAMES.COLUMNS.ID}
	`;

	const mainSqlQueryWithLikesJoin = `
		${mainSqlQuery}
		LEFT JOIN ${DB_NAMES.TABLES.REVIEWS_USERS_LIKES} as likes_t
		ON reviews_t.${DB_NAMES.COLUMNS.ID} = likes_t.${DB_NAMES.COLUMNS.REVIEW_ID}
	`;

	const mainSqlQueryWithLikesAndWhereOperator = `
		${mainSqlQueryWithLikesJoin}
		${whereOperator}
	`;

	const mainSqlQueryWithWhereOperator = `
		${mainSqlQuery}
		${whereOperator}
	`;

	const query = `
		SELECT
		reviews_t.${DB_NAMES.COLUMNS.ID},
		reviews_t.${DB_NAMES.COLUMNS.TEXT},
		reviews_t.${DB_NAMES.COLUMNS.RATE},
		reviews_t.${DB_NAMES.COLUMNS.LONGITUDE},
		reviews_t.${DB_NAMES.COLUMNS.LATITUDE},
		reviews_t.${DB_NAMES.COLUMNS.CREATED_AT},
		numbers_t.${DB_NAMES.COLUMNS.CAR_NUMBER},
		reviews_t.${DB_NAMES.COLUMNS.CAR_NUMBER_ID},
		reviews_t.${DB_NAMES.COLUMNS.USER_NAME},
		SUM(likes_t.${DB_NAMES.COLUMNS.IS_LIKED}) as likes,
		SUM(likes_t.${DB_NAMES.COLUMNS.IS_DISLIKED}) as dislikes

		${mainSqlQueryWithLikesAndWhereOperator}

		GROUP BY reviews_t.${DB_NAMES.COLUMNS.ID}
		ORDER BY reviews_t.${DB_NAMES.COLUMNS.CREATED_AT} DESC
		${limitOperator};

		SELECT COUNT(*) as count ${mainSqlQueryWithWhereOperator};
	`;

	const [[_items, [{ count }]]] = await initMysql().query(query);

	const items = _items.map(({ likes, dislikes, ...item }) => ({ ...item, likes: likes || 0, dislikes: dislikes || 0 }));

	return { items, count };
}