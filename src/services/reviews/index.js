import Service from '..';
import { DB_NAMES } from '../../common/constants/mysql';
import CarNumbersService from '../carNumbers/index';
import { isNullOrUndefined, parseCarNumber, parseRate } from '../../common/utils/helpers';
import { getReviews } from './getReviews';
import LocationService from '../location';
import LikesDislikesService from '../likesDislikes';
import { GENERATED_ERRORS } from '../../common/constants/errors';
import initMysql from '../mysqlConnection';

const carNumbersService = new CarNumbersService();
export default class ReviewsService extends Service {
	locationService = new LocationService();
	likesDislikesService = new LikesDislikesService();

	async getReviewWithLocationString(review) {
		const {
			location_latitude: latitude,
			location_longitude: longitude,
		} = review;

		if (latitude && longitude) {
			const locationText = await this.locationService.getLocationString({ longitude, latitude });

			return {
				...review,
				location: {
					longitude,
					latitude
				},
				locationText,
			};
		}

		return review;
	}

	async getReviews(params) {
		const { items, count } = await getReviews(params);

		// const promisedReviews = await Promise.all(reviews.map(async review => {
		// 	const parsedReview = await this.getReviewWithLocationString(review);

		// 	return parsedReview;
		// }));

		// return promisedReviews;

		const reviewsItems = items.map(review => {
			const {
				location_latitude: latitude,
				location_longitude: longitude,
			} = review;

			if (latitude && longitude) {
				const locationLink = this.locationService.getLocationLink({ longitude, latitude });

				return {
					...review,
					location: {
						longitude,
						latitude
					},
					locationLink,
				};
			}

			return review;
		});

		return { items: reviewsItems, count };
	}

	async addReview(review) {
		const {
			carNumber: _carNumber,
			rate: _rate,
			text,
			userName,
			location
		} = review;

		const { latitude, longitude } = location || {};

		let carNumberId;
		const carNumber = parseCarNumber(_carNumber);
		const rate = parseRate(_rate);

		try {
			const { id } = await carNumbersService.getCarNumber(carNumber)
			carNumberId = id;
		} catch (error) {
			const { insertId } = await carNumbersService.addCarNumber(carNumber);
			carNumberId = insertId;
		}

		const columns = [
			DB_NAMES.COLUMNS.CAR_NUMBER_ID,
			DB_NAMES.COLUMNS.TEXT,
			DB_NAMES.COLUMNS.RATE
		];

		const values = [carNumberId, text, rate];

		if (userName) {
			columns.push(DB_NAMES.COLUMNS.USER_NAME);
			values.push(userName);
		}

		if (longitude && latitude) {
			columns.push(DB_NAMES.COLUMNS.LATITUDE, DB_NAMES.COLUMNS.LONGITUDE);
			values.push(latitude, longitude);
		}

		const res = this.insertInstance(
			DB_NAMES.TABLES.REVIEWS,
			columns,
			values,
		);

		return res;
	}

	async likeDislikeReview(reviewId, userId, like = true) {
		await initMysql().query('START TRANSACTION');

		const likesDislikes = await this.likesDislikesService
			.getReviewStatusByUserId(reviewId, userId);

		const { is_liked = 0, is_disliked = 0 } = likesDislikes || {};

		let isLiked = 0;
		let isDisliked = 0;

		if (like) {
			isLiked = !is_liked;

			if (is_disliked) {
				isDisliked = !is_disliked;
			}
		}

		if (!like) {
			isDisliked = !is_disliked;

			if (is_liked) {
				isLiked = !is_liked;
			}
		}

		isLiked = Number(isLiked);
		isDisliked = Number(isDisliked);

		if (isNullOrUndefined(likesDislikes)) {
			await this.insertInstance(
				DB_NAMES.TABLES.REVIEWS_USERS_LIKES,
				[DB_NAMES.COLUMNS.USER_ID, DB_NAMES.COLUMNS.REVIEW_ID, DB_NAMES.COLUMNS.IS_LIKED, DB_NAMES.COLUMNS.IS_DISLIKED],
				[userId, reviewId, isLiked, isDisliked]
			);

			await initMysql().query('COMMIT');

			return;
		}

		const whereClause = `
			WHERE ${DB_NAMES.COLUMNS.USER_ID} = '${userId}'
			AND ${DB_NAMES.COLUMNS.REVIEW_ID} = '${reviewId}'
		`;

		await this.updateInstance(
			DB_NAMES.TABLES.REVIEWS_USERS_LIKES,
			[DB_NAMES.COLUMNS.IS_LIKED, DB_NAMES.COLUMNS.IS_DISLIKED],
			[isLiked, isDisliked],
			whereClause,
		);

		await initMysql().query('COMMIT');
	}
}