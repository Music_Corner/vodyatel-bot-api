import axios from 'axios';

import Service from '..';

export default class LocationService extends Service {
	async getLocationString({ longitude, latitude }) {
		const params = {
			key: '57bdbb99b124482baa7dcc223d247f85',
			q: `${latitude}+${longitude}`,
		};

		const { data } = await axios.get('https://api.opencagedata.com/geocode/v1/json', { params });

		/* https://opencagedata.com/api */
		if (data.results.length) {
			const [{ formatted }]	= data.results;

			return formatted || '';
		}

		return '';
	}

	getLocationLink ({ longitude, latitude }) {
		return `http://www.google.com/maps/place/${latitude},${longitude}`;
	}
}