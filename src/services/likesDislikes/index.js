import Service from '..';
import { DB_NAMES } from '../../common/constants/mysql';

export default class LikesDislikesService extends Service {
	async getReviewStatusByUserId(reviewId, userId) {
		const whereClause = `${DB_NAMES.COLUMNS.USER_ID} = '${userId}'
			AND ${DB_NAMES.COLUMNS.REVIEW_ID} = '${reviewId}'
		`;

		const [likesDislikes] = await this.getInstances(DB_NAMES.TABLES.REVIEWS_USERS_LIKES, 1, 0, whereClause);

		return likesDislikes;
	}
}