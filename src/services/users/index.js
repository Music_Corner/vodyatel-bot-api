import jwt from 'jsonwebtoken';

import Service from '..';
import { ENV } from '../../common/constants/env';
import Model from '../../models';
import PrivateUserModel from '../../models/users/PrivateUserModel';
import PublicUserModel from '../../models/users/PublicUserModel';

import { createUser } from './createUser';
import { updatePublicUserInfo } from './updatePublicUserInfo';
import { getUserInfo } from './getUserInfo';
import { getUsers } from './getUsers';

export default class UsersService extends Service {
	async createUser(userInfo) {
		const validUserInfo = Model.squashModels(PrivateUserModel, PublicUserModel)(userInfo);

		const result = await createUser(validUserInfo);

		return result;
	}

	async getUsers(column, value) {
		const result = await getUsers(column, value);

		return result;
	}

	async getUserInfo(id) {
		const results = await getUserInfo(id);

		return results;
	}

	async updatePublicUserInfo(form, userId) {
		const publicUserValidForm = PublicUserModel.validate(form);

		await updatePublicUserInfo(publicUserValidForm, userId);
	}

	async generateToken(userData) {
		return jwt.sign(userData, process.env.JWT_SECRET, { expiresIn: process.env.ACCESS_EXPIRATION });
	}
}
