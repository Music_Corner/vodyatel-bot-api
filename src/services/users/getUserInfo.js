import { DB_NAMES } from '../../common/constants/mysql';
import PublicUserModel from '../../models/users/PublicUserModel';
import initMysql from '../mysqlConnection';

export const getUserInfo = async (id) => {
	const [users = []] = await initMysql().query(`
		SELECT * FROM ${DB_NAMES.TABLES.USERS_INFO}
		WHERE user_id = "${id}"
	`);

	if (users.length) {
		return new PublicUserModel(users[0]);
	}

	return null;
};
