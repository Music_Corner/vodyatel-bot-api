import Service from '..';
import { DB_NAMES } from '../../common/constants/mysql';

export default class CarNumbersService extends Service {
	async getCarNumbers(page, limit) {
		const data = await this.getInstances(DB_NAMES.TABLES.CAR_NUMBERS, page, limit);

		return data;
	}

	async getCarNumber(carNumber) {
		const data = await this.getSingleInstanceByColumn(
			DB_NAMES.TABLES.CAR_NUMBERS,
			DB_NAMES.COLUMNS.CAR_NUMBER,
			carNumber
		);

		return data;
	}

	async addCarNumber(carNumber) {
		const res = await this.insertInstance(
			DB_NAMES.TABLES.CAR_NUMBERS, [DB_NAMES.COLUMNS.CAR_NUMBER], [carNumber]
		);

		return res;
	}
}
