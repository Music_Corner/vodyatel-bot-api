import { hash } from 'bcrypt';

import { DEFAULT_LIMIT } from '../constants/mysql';

export const getOffset = (page = 0, limit = DEFAULT_LIMIT) => page * limit;

export const getLimitOperator = (page, limit) => {
	const offset = getOffset(page, limit);
	return limit ? `LIMIT ${offset}, ${limit}` : '';
}

export const hashPassword = password => hash(password, 8);

export const parseCarNumber = (carNumber = '') => carNumber.replace(/\s/g, '').toLowerCase();

export const parseRate = _rate => {
	let rate = Math.round((isNaN(Number(_rate)) ? 0 : Number(_rate)));

	if (rate > 5) {
		rate = 5
	}

	if (rate > 5) {
		rate = 0;
	}

	return rate;
};

export const isNullOrUndefined = (value) => value === null || value === undefined;
