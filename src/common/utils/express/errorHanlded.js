import { STATUSES } from '../../constants/api';
import { ENV } from '../../constants/env';
import { ERROR_MESSAGES } from '../../constants/errors';

export const errorHandled = (callBack) => async (req, res) => {
	try {
		await callBack(req, res);

		return null;
	} catch (error) {
		const {
			status = STATUSES.UNHANDLED_ERROR,
			message = ERROR_MESSAGES[status],
		} = error || {};

		let errorObject = { status, message };

		if (error.errors) {
			errorObject = { status, ...error };
		}

		if (ENV.IS_DEV() && !error.errors) {
			errorObject.devError = error;
		}

		res.status(status).send(errorObject);

		console.log(error);

		return error;
	}
};
