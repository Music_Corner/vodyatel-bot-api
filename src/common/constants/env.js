export const ENV = {
	DEV_ENV: 'development',
	PROD_ENV: 'production',
	IS_DEV() {
		return process.env.NODE_ENV === this.DEV_ENV;
	},
};
