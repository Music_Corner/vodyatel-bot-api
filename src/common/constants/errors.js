import { STATUSES } from './api';

export const MYSQL_ERR_CODES = {
	ER_DUP_ENTRY: 'ER_DUP_ENTRY',
};

export const API_ERROR_CODES = {
	SECURITY: {
		MAIN: 'security_error',
		INVALID_ACCESS: 'sec.access_token_invalid',
		INVALID_AUTH_DATA: 'sec.invalid_auth_data',
	},
	FORM: {
		VALIDATION: 'form.validation',
	},
	COMMON: {
		UNPROCESSABLE_ENTITY: 'unprocessable_entity',
		RESOURCE_ALREADY_EXISTS: 'common.resource_already_exists',
		NOT_ALLOWED: 'not_allowed',
	},
};

export const ERROR_MESSAGES = {
	VALIDATION_ERROR: 'Validation error',
	REQUIRED_FIELD_IS_EMPTY: 'This field is required',
	PASSWORDS_ARE_NOT_SAME: 'Passwords do not match',

	[STATUSES.AUTHORIZATION_ERROR]: 'Invalid authorization',
	[STATUSES.UNHANDLED_ERROR]: 'Something went wrong',

	getMysqlErrMessage(err) {
		const { message = '', code } = err;

		const ERRORS = {
			get [MYSQL_ERR_CODES.ER_DUP_ENTRY]() {
				const [value, _field] = message.replace('Duplicate entry ', '').replace(/'/g, '').split(' for key ');
				const field = _field.replace(/.*_*._/gm, '');

				return GENERATED_ERRORS.RESOURCE_ALREADY_EXISTS(field, value);
			},
		};

		return ERRORS[code];
	},
};

export const GENERATED_ERRORS = {
	UNPROCESSABLE_ENTITY: {
		message: 'No such resource',
		status: STATUSES.UNPROCESSABLE_ENTITY,
		code: API_ERROR_CODES.COMMON.UNPROCESSABLE_ENTITY,
		errors: [{
			message: 'No such resource',
			status: STATUSES.UNPROCESSABLE_ENTITY,
			code: API_ERROR_CODES.COMMON.UNPROCESSABLE_ENTITY,
		}],
	},
	NOT_ALLOWED: {
		message: 'Not allowed',
		status: STATUSES.NOT_ALLOWED,
		code: API_ERROR_CODES.COMMON.NOT_ALLOWED
	},
	RESOURCE_ALREADY_EXISTS: (field, value) => ({
		message: `${field} already used: ${value}`,
		status: STATUSES.UNPROCESSABLE_ENTITY,
		code: API_ERROR_CODES.COMMON.UNPROCESSABLE_ENTITY,
		errors: [{
			message: `${field} already used: ${value}`,
			field,
			code: API_ERROR_CODES.COMMON.RESOURCE_ALREADY_EXISTS,
		}],
	})
};