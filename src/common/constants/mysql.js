export const DB_NAMES = {
	TABLES: {
		USERS: 'users',
		USERS_INFO: 'users_info',

		REVIEWS: 'reviews',
		CAR_NUMBERS: 'car_numbers',
		USERS_CAR_NUMBERS: 'users_car_numbers',
		REVIEWS_USERS_LIKES: 'reviews_users_likes',
	},
	COLUMNS: {
		ID: 'id',
		NAME: 'name',
		CREATED_AT: 'created_at',
		UPDATED_AT: 'updated_at',

		IS_REMOVED: 'is_removed',
		IS_LIKED: 'is_liked',
		IS_DISLIKED: 'is_disliked',

		USER_ID: 'user_id',
		REVIEW_ID: 'review_id',

		EMAIL: 'email',
		PASSWORD: 'password',

		PHONE: 'phone',
		FIRST_NAME: 'first_name',
		LAST_NAME: 'last_name',

		CAR_NUMBER: 'car_number',
		TEXT: 'text',
		RATE: 'rate',
		USER_NAME: 'user_name',

		CAR_NUMBER_ID: 'car_number_id',

		LONGITUDE: 'location_longitude',
		LATITUDE: 'location_latitude',
		LIKES: 'likes',
		DISLIKES: 'dislikes',
	},
};

export const DEFAULT_LIMIT = 30;
