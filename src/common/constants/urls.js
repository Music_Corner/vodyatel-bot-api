export const API_METHODS = {
	AUTH: '/auth',
	get REGISTER() {
		return `${this.AUTH}/register`;
	},
	get LOGIN() {
		return `${this.AUTH}/token`;
	},
	USERS: '/users',

	REVIEWS: '/reviews',

	get REVIEW() {
		return `${this.REVIEWS}/:id`;
	},

	get LIKE() {
		return `${this.REVIEWS}/:id/like`;
	},

	get DISLIKE() {
		return `${this.REVIEWS}/:id/dislike`;
	},

	TOKEN: '/token',
	REFRESH_TOKEN: '/refresh',

	LOCATION: '/location',
};
