import { API_METHODS } from '../../common/constants/urls';
import { errorHandledWithAuthorization } from '../../common/utils/express/errorHandledWithAuthorization';
import expressServer from '../../common/utils/express/initExpressServer';
import LocationController from '../../controllers/location'

export const initLocationRouting = () => {
	const locationController = new LocationController();

	expressServer.get(API_METHODS.LOCATION, errorHandledWithAuthorization(async (req, res) => {
		const result = await locationController.getLocationString(req, res);

		res.send({ status: 200, data: result });
	}));
}