import initAuthRouting from './auth';
import { initLocationRouting } from './location';
import initReviewsRouting from './reviews';

export const initRouting = () => {
	initAuthRouting();
	initReviewsRouting();
	initLocationRouting();
};
