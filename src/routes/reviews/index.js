import { API_METHODS } from '../../common/constants/urls';
import { errorHandledWithAuthorization } from '../../common/utils/express/errorHandledWithAuthorization';
import expressServer from '../../common/utils/express/initExpressServer';
import ReviewsController from '../../controllers/reviews';

const initReviewsRouting = () => {
	const reviewsController = new ReviewsController();

	expressServer.get(API_METHODS.REVIEWS, errorHandledWithAuthorization(async (req, res) => {
		const result = await reviewsController.getReviews(req, res);

		res.send({ status: 200, data: result || [] });
	}));

	expressServer.get(API_METHODS.REVIEW, errorHandledWithAuthorization(async (req, res) => {
		const data = await reviewsController.getReviewById(req, res);

		res.send({ status: 200, data });
	}));

	expressServer.post(API_METHODS.REVIEWS, errorHandledWithAuthorization(async (req, res) => {
		await reviewsController.addReview(req, res);

		res.send({ status: 200, data: 'success' });
	}));

	expressServer.post(API_METHODS.LIKE, errorHandledWithAuthorization(async (req, res) => {
		await reviewsController.likeDislikeReview(req, res);

		res.send({ status: 200, data: 'success' });
	}));

	expressServer.post(API_METHODS.DISLIKE, errorHandledWithAuthorization(async (req, res) => {
		await reviewsController.likeDislikeReview(req, res, false);

		res.send({ status: 200, data: 'success' });
	}));
};

export default initReviewsRouting;
