import { STATUSES } from '../common/constants/api';
import { API_ERROR_CODES, ERROR_MESSAGES } from '../common/constants/errors';

export default class Model {
	/**
	 * 
	 * @param {Object} modelObj 
	 */
	normalizeToFront(modelObj) {
		const normalizedObj = {};
		const toCamel = (s) => {
			return s.replace(/([-_][a-z])/ig, ($1) => {
				return $1.toUpperCase()
					.replace('-', '')
					.replace('_', '');
			});
		};

		Object.keys(modelObj).forEach(key => {
			const value = modelObj[key];

			normalizedObj[toCamel(key)] = value;
		});

		return normalizedObj;
	}

	static normalizeToFront(modelObj) {
		const normalizedObj = {};

		Object.keys(modelObj).forEach(key => {
			const value = modelObj[key];

			normalizedObj[key.replace('_', '$1'.toUpperCase())] = value;
		});

		return normalizedObj;
	}

	static getModelError(validationMap, modelObj = {}) {
		const errors = Object.keys(validationMap).map(field => {
			const value = modelObj[field];
			const validationObj = validationMap[field];

			const {
				isRequired = false,
				regexp,
				maxLength,
				minLength,
			} = validationObj;

			const err = { field, code: API_ERROR_CODES.FORM.VALIDATION };

			if (isRequired && !value) {
				return { ...err, message: ERROR_MESSAGES.REQUIRED_FIELD_IS_EMPTY };
			}

			/* If value exists - check other rules */
			if (value) {
				if (regexp && !regexp.test(String(value))) {
					return { ...err, message: ERROR_MESSAGES.VALIDATION_ERROR };
				}

				if (value.length) {
					if (maxLength && value.length > maxLength) {
						return { ...err, message: ERROR_MESSAGES.VALIDATION_ERROR };
					}

					if (minLength && value.length < minLength) {
						return { ...err, message: ERROR_MESSAGES.VALIDATION_ERROR };
					}
				}
			}
		}).filter(err => err);

		if (errors.length) {
			return {
				message: ERROR_MESSAGES.VALIDATION_ERROR,
				code: API_ERROR_CODES.FORM.VALIDATION,
				status: STATUSES.VALIDATION_ERROR_STATUS,
				errors
			};
		}
	}

	static squashModels(...models) {
		return (data = {}) => {
			let validModel;
			let error;

			models.forEach(model => {
				try {
					validModel = { ...validModel, ...model.validate(data) };
				} catch (err) {
					const _errors = (error || {}).errors;

					const errors =  [...(_errors || []), ...(err.errors || [])];
		
					error = { ...(error || {}), ...err, errors };
				}
			});

			if (error) throw error;

			return validModel;
		};
	}
}
