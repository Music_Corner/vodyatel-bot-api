import Model from '..';

export default class PublicUserModel extends Model {
	constructor(user) {
		const {
			first_name,
			last_name,
			phone,
		} = user;

		super(user);

		return this.normalizeToFront({
			first_name,
			last_name,
			phone,
		});
	}

	static validate(user) {
		const {
			firstName,
			lastName,
			phone,
		} = user;

		const validationMap = {
			firstName: {
				isRequired: true,
			},
			lastName: {
				isRequired: true,
			},
		};

		const error = super.getModelError(validationMap, user);

		if (error) throw error;

		return {
			firstName,
			lastName,
			phone,
		}
	}
}
