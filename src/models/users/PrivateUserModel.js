import Model from '..';
import { VALIDATION_REGEXP } from '../../common/constants/validation';

export default class PrivateUserModel extends Model {
	constructor(user) {
		const {
			id,
			email,
			password,
		} = user;

		return {
			id,
			email,
			password,
		};
	}

	static validate(user) {
		const {
			login,
			email,
		} = user;

		const validationMap = {
			login: {
				isRequired: true,
				regexp: VALIDATION_REGEXP.EMAIL,
			},
			password: {
				isRequired: true,
			},
		}

		const _user = {
			...user,
			login: login || email,
		};

		const error = this.getModelError(validationMap, _user);

		if (error) throw error;

		return _user;
	}
}
