import Model from '..';

export default class ReviewModel extends Model {
	constructor(review) {
		super(review);

		return this.normalizeToFront(review);
	}
}
